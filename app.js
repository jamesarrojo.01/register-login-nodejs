// [SECTION] Packages and Dependencies
    const express = require("express")
    const mongoose = require("mongoose")
    const dotenv = require("dotenv")
    const cors = require("cors")

    const userRoutes = require('./routes/users')
    const evenRoutes = require('./routes/events')
    const gameServerRoutes = require('./routes/gameServers')
    const gameClientRoutes = require('./routes/gameClients')
// [SECTION] Server Setup
    const app = express()
    dotenv.config()
    app.use(cors())
    app.use(express.json())
    const secret = process.env.CONNECTION_STRING
    const port = process.env.PORT || 4000

// [SECTION] Application Routes
    app.use('/users', userRoutes)
    app.use('/events', evenRoutes)
    app.use('/gameServers', gameServerRoutes)
    app.use('/gameClients', gameClientRoutes)

// [SECTION] Database Connection
    mongoose.connect(secret)
    let connectionStatus = mongoose.connection
    connectionStatus.on('open', () => console.log('Database is connected'))

// [SECTION] Gateway Response
    app.get('/', (req, res) => {
        res.send("API by [Web] James")
    })
    app.listen(port, () => console.log("Server is running on port:", port))