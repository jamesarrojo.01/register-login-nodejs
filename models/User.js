// [SECTION] Dependencies and Modules
    const mongoose = require('mongoose');

// [SECTION] Blueprint Schema
    const userSchema = new mongoose.Schema({
        firstName: {
            type: String,
            required: [true, 'First Name is required']
        },
        lastName: {
            type: String,
            required: [true, 'Last Name is required']
        },
        email: {
            type: String,
            required: [true, 'Email is required']
        },
        password: {
            type: String,
            required: [true, 'Password is required']
        },
        isAdmin: {
            type: Boolean,
            default: false
        },
        clothesprofile: {
            type: Object,
            "default" : {
                
                "MetaData" : {
                    "Name": String,
                    "Anatomy": "Human Male Adult"
                },
                "Basebody": {
                    "Age": 0,
                    "Size": 0.8556985259056091,
                    "Skin": {
                        "MaterialIndex": 0,
                        "ScalarParameters": {
                            "leftNostril_up": 0,
                            "rightNostril_up": 0,
                            "Lips Roughness": 1.5,
                            "Eye Roughness": 1,
                            "browOuterUpLeft": 0,
                            "browOuterUpRight": 0,
                            "eyeSquintLeft": 0,
                            "eyeSquintRight": 0,
                            "mouthLeft": 0,
                            "mouthRight": 0,
                            "mouthShrugLower": 0,
                            "browInnerUp": 0,
                            "noseSneerRight": 0,
                            "mouthPressLeft": 0,
                            "mouthPressRight": 0,
                            "noseSneerLeft": 0,
                            "fear": 0,
                            "leftCheek_out": 0,
                            "rightCheek_out": 0,
                            "mouthStretchLeft": 0,
                            "mouthStretchRight": 0,
                            "Show Alternative Textures": 0
                        },
                        "HDRVectorParameters": {
                            "Skin Tint": {
                                "H": 21.329437255859375,
                                "S": 0.7395830154418945,
                                "V": 1,
                                "A": 0.30000001192092896,
                                "Intensity": 1
                            },
                            "Lips Tint": {
                                "H": 0,
                                "S": 1,
                                "V": 0.17708300054073334,
                                "A": 0,
                                "Intensity": 1
                            },
                            "Eye Tint": {
                                "H": 0,
                                "S": 1,
                                "V": 0.17708300054073334,
                                "A": 0,
                                "Intensity": 1
                            }
                        }
                    },
                    "Eyes": {
                        "ScalarParameters": {
                            "Iris UV Radius": 0.17499999701976776,
                            "Pupil Scale": 0.20000000298023224
                        },
                        "HDRVectorParameters": {
                            "Iris Hue Tint": {
                                "H": 219.951904296875,
                                "S": 0.8013486266136169,
                                "V": 0.5028859972953796,
                                "A": 1,
                                "Intensity": 1
                            }
                        }
                    },
                    "MorphTargets": {
                        "face_None": 0,
                        "face_02": 0,
                        "face_03": 0,
                        "face_04": 0,
                        "face_05": 0,
                        "face_06": 1,
                        "face_asian": 0,
                        "body_weight": 0,
                        "body_muscles": 0,
                        "eyes_height": 0,
                        "eyes_depth": 0,
                        "eyes_size": 0,
                        "nose_height": 0,
                        "nose_depth": 0,
                        "nose_tip": 0,
                        "nose_size": 0,
                        "nose_tip_angle": 0,
                        "nose_curve": 0,
                        "mouth_height": 0,
                        "mouth_depth": 0,
                        "mouth_nasolabial_fold": 0,
                        "mouth_size": 0,
                        "jaw_height": 0,
                        "jaw_width": 0,
                        "jaw_underbite": 0,
                        "chin_height": 0,
                        "chin_width": 0,
                        "chin_size": 0
                    },
                    "MorphTargetGroups": {
                        "face": 5
                    }
                },
                "Hairstyle": {
                    "DataAssets": [],
                    "GlobalScalarParameters": {},
                    "GlobalHDRVectorParameters": {
                        "RootColor": {
                            "H": 21.9705810546875,
                            "S": 0.7521071434020996,
                            "V": 0.078186996281147,
                            "A": 1,
                            "Intensity": 1
                        },
                        "TipColor": {
                            "H": 21.9705810546875,
                            "S": 0.7521071434020996,
                            "V": 0.078186996281147,
                            "A": 1,
                            "Intensity": 1
                        }
                    }
                },
                "Apparel": {
                    "DataAssets": [
                        {
                            "DataAsset": "BP_CDA_Apparel_UpperBody_C'/Game/CharacterEditor/CharacterParts/DataAssets/UpperBody/CDA_ma_tanktop.CDA_ma_tanktop'",
                            "MaterialVariantIndex": -1
                        },
                        {
                            "DataAsset": "BP_CDA_Apparel_LowerBody_C'/Game/CharacterEditor/CharacterParts/DataAssets/LowerBody/CDA_ma_pants_short_casual.CDA_ma_pants_short_casual'",
                            "MaterialVariantIndex": -1
                        },
                        {
                            "DataAsset": "BP_CDA_Apparel_Feet_C'/Game/CharacterEditor/CharacterParts/DataAssets/Feet/CDA_ma_feet_sneaker.CDA_ma_feet_sneaker'",
                            "MaterialVariantIndex": 0
                        }
                    ],
                    "GlobalScalarParameters": {},
                    "GlobalHDRVectorParameters": {}
                },
                "Equipment": {
                    "DataAssets": [],
                    "GlobalScalarParameters": {},
                    "GlobalHDRVectorParameters": {}
                },
                "Attachments": {
                    "DataAssets": [],
                    "GlobalScalarParameters": {},
                    "GlobalHDRVectorParameters": {}
                }
            }
        },

        

    })

    userSchema.pre('save', function(next) {
        this.clothesprofile["MetaData"]["Name"] = this.email;
        next();
    })

// [SECTION] Model
    const User = mongoose.model('User', userSchema)
    module.exports = User;
