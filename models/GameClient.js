// [SECTION] Dependencies and Modules
const mongoose = require('mongoose');

// [SECTION] Blueprint Schema
    const gameClientSchema = new mongoose.Schema({
        ipAddress: {
            type: String,
            required: [true, 'Title is required']
        },
        isAvailable: {
            type: Boolean,
            default: true
        },
        isLoggingOut: {
            type: Boolean,
            default: false
        }

    })

// [SECTION] Model
    const GameClient = mongoose.model('GameClient', gameClientSchema)
    module.exports = GameClient;
