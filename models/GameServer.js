// [SECTION] Dependencies and Modules
const mongoose = require('mongoose');

// [SECTION] Blueprint Schema
    const gameServerSchema = new mongoose.Schema({
        ipAddress: {
            type: String,
            required: [true, 'Title is required']
        },
        isAvailable: {
            type: Boolean,
            default: true
        }

    })

// [SECTION] Model
    const GameServer = mongoose.model('GameServer', gameServerSchema)
    module.exports = GameServer;
