// [SECTION] Dependencies and Modules
const mongoose = require('mongoose');

// [SECTION] Blueprint Schema
    const eventSchema = new mongoose.Schema({
        title: {
            type: String,
            required: [true, 'Title is required']
        },
        description: {
            type: String,
            required: [true, 'Description is required']
        },
        headline: {
            type: String,
            required: [true, 'Headline is required']
        },
        imgUrl: {
            type: String,
            required: [true, 'Image URL is required']
        }

    })

// [SECTION] Model
    const Event = mongoose.model('Event', eventSchema)
    module.exports = Event;
