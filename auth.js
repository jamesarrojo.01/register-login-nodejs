// Dependencies
    const jwt = require("jsonwebtoken")
    const dotenv = require("dotenv")
    dotenv.config()
// Token Creation
    module.exports.createAccessToken = (user) => {
        const data = {
            id: user._id,
            isAdmin: user.isAdmin,
            fullName: `${user.firstName} ${user.lastName}`
        }

        return jwt.sign(data, process.env.ACCESS_TOKEN_SECRET, {})
    }

// User Authentication
    module.exports.verify = (req, res, next) => {
        let token = req.headers.authorization

        // console.log(token)

        if (token) {
            token = token.slice(7, token.length)

            jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, decodedToken) => {
                if (err) {
                    return res.send({
                        auth: "Failed",
                        message: err.message
                    })
                } else {
                    req.user = decodedToken

                    next()
                }
            })
        } else {
            return res.send({auth: "Failed. No Token"})
        }
    }
// isAdmin ?
    module.exports.verifyAdmin = (req, res, next) => {
        console.log(req.user)
        if (req.user.isAdmin) {
            next()
        } else {
            return res.status(400).json({
                auth: "Failed",
                message: "Action Forbidden"
            })
        }
    }