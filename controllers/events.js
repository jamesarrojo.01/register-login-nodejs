// [SECTION] Dependencies and Modules
    const Event = require("../models/Event")

// [SECTION] Functionalities [CREATE]
    module.exports.createEvent = async (req, res) => {
        try {
            const event = await Event.findOne({title: req.body.title})
            if (event) {

                if (event.title === req.body.title) {

                    return res.status(400).json({
                        message: "Event already exists!"
                    })
                }

            } else {
                const newEvent = new Event({
                    title: req.body.title,
                    description: req.body.description,
                    imgUrl: req.body.imgUrl,
                    headline: req.body.headline
                })
                
                const savedEvent = await newEvent.save()

                return res.send(savedEvent)
            }
        } catch (err) {
            res.send(err)
        }
    }

// [SECTION] RETRIEVE
    module.exports.getAllEvent = async (req, res) => {
        try {
            const events = await Event.find({})
            return res.send(events)
        } catch (err) {
            return res.send(err)
        }
    }