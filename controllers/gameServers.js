// [SECTION] Dependencies and Modules
    const GameServer = require("../models/GameServer")

// [SECTION] Functionalities [CREATE]
    module.exports.addServer = async (req, res) => {

        try {

            const ipList = req.body.ipAddress.split(", ")
            
            for (const ip of ipList) {

                const server = await GameServer.findOne({ipAddress: ip})
                
                if (!server) {
                    const newServer = new GameServer({
                        ipAddress: ip
                    })
    
                    await newServer.save()
                    
                } else {
                    console.log(`${ip} is already in the database`)
                }
            }

            return res.send({message: "Successfully loaded servers"})

        } catch (err) {
            return res.send(err)
        }
    }

// [SECTION] RETRIEVE
    // get all servers
    module.exports.getAllServer = async (req, res) => {
        try {
            const servers = await GameServer.find({})

            return res.send(servers)
        } catch (err) {
            res.send(err)
        }
    }

    // get all available servers
    module.exports.getAllAvailableServer = async (req, res) => {
        try {
            const servers = await GameServer.find({isAvailable: true})

            return res.send(servers)
        } catch (err) {
            res.send(err)
        }
    }

    // get ONE available server
    module.exports.getAvailableServer = async (req, res) => {
        try {
            const server = await GameServer.findOne({isAvailable: true})

            res.send(server)
        } catch (err) {
            res.send(err)
        }
    }

// [SECTION] UPDATE
    // set server to be available
    module.exports.setAvailable = async (req, res) => {
        try {

            const server = await GameServer.findOneAndUpdate({ipAddress: req.params.ipAddress}, {isAvailable: true})
            res.send({message: "Server IP is now available"})

        } catch (err) {
            res.send(err)
        }
    }

    // set server to be unavailable
    module.exports.setUnavailable = async (req, res) => {
        try {

            const server = await GameServer.findOneAndUpdate({ipAddress: req.params.ipAddress}, {isAvailable: false})
            res.send({message: "Server IP is now being used"})
            
        } catch (err) {
            res.send(err)
        }
    }

// [SECTION] DELETE
module.exports.deleteServer = async (req, res) => {
    try {
        await GameServer.findOneAndDelete({ipAddress: req.params.ipAddress})
        res.send({message: "Game server IP is now deleted"})
    } catch (err) {
        res.send(err)
    }
}