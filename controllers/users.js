// [SECTION] Dependencies and Modules
    const User = require("../models/User")
    const bcrypt = require("bcrypt")
    const dotenv = require("dotenv")
    const auth = require("../auth")

// [SECTION] Environment Setup
    dotenv.config()
    const salt = Number(process.env.SALT)

// [SECTION] Functionalities [CREATE]
    module.exports.registerUser = async (req, res) => {

        if(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(req.body.email) === false) {
            return res.status(400).json({
                
                message: "Invalid credentials",
                code: "10"
            })
        }

        try {
            const user = await User.findOne({                
                email: req.body.email
            })
    
            if (user) {
                let errors = {}
                if (user.email === req.body.email) {
                    errors.message = `${user.email} is already used`
                    errors.code = "10"
                } 
                return res.status(400).json(errors)
            } else {
                const newUser = new User({
                    firstName: req.body.firstName,
                    lastName: req.body.lastName,
                    email: req.body.email,
                    password: bcrypt.hashSync(req.body.password, salt)
                })
                const savedUser = await newUser.save();
                
                const data = {
                  
                    message: "Account registration successful",
                    code: "2"
                   
                }
                res.send(data)
    
            }
        } catch (err) {
            return res.status(500).json({
                error: err
            })
        }
    }

    // Login
    module.exports.loginUser = (req, res) => {
        if(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(req.body.email) === false) {
            return res.status(401).json({
        
                message: "Invalid credentials",
                code: "10"

            })
        }
        User.findOne({email: req.body.email})
        .then(foundUser => {
            if (foundUser === null) {
                
                return res.status(401).json({message: "Invalid credentials", code: "10"})
            } else {


                const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password)

                if (isPasswordCorrect) {
                    console.log(foundUser)
                    return res.send({
                        id: foundUser._id,
                        email: foundUser.email,
                        firstName: foundUser.firstName,
                        lastName: foundUser.lastName,
                        message: "Successfully logged in",
                        token: auth.createAccessToken(foundUser),
                        clothesprofile: foundUser.clothesprofile,
                        // "MetaData": foundUser["MetaData"],
                        // "Basebody": foundUser["Basebody"],
                        // "Hairstyle": foundUser["Hairstyle"],
                        // "Apparel": foundUser["Apparel"],
                        // "Equipment": foundUser["Equipment"],
                        // "Attachments": foundUser["Attachments"]
                    })
                } else {
                    return res.status(401).json({
                        message: "Invalid credentials",
                        code: "10"
                    })
                }
            }
        })
        .catch(err => res.status(400).json(err))
    }

// [SECTION] RETRIEVE
    // private to user
    module.exports.getUserDetails = async (req, res) => {

        try {
            const user = await User.findById(req.user.id)
            const data = {
                username: user.username,
                email: user.email,
                id: user._id
            }
            res.send(data)
        } catch (err) {
            res.send(err)
        }
    }

    // public
    module.exports.getPublicUserDetails = async (req, res) => {
        console.log(req.params.id)
        try {
            const user = await User.findById(req.params.id)
            const data = {
                firstName: user.firstName,
                lastName: user.lastName,
                id: user._id
            }
            res.send(data)
        } catch (err) {
            res.send(err)
        }
    }

    // for the clothes
    module.exports.getCharacterProfile = async (req, res) => {
        try {
            const characterProfile = await User.findOne({email: req.params.email})
            res.send(characterProfile.clothesprofile)
        } catch (err) {
            res.send(err)
        }
    }

// [SECTION] UPDATE
    module.exports.saveCharacterProfile = async (req, res) => {
        console.log(req.body.email)

        
        
        try {
            if (!req.body) res.status(400).send({
                message: "json string for character profile was not found"
            })
            if (!req.body["MetaData"]["Name"]) res.status(400).send({
                message: "Email not found"
            })
    
            const profile = await User.findOneAndUpdate({email: req.body["MetaData"]["Name"]}, {
                clothesprofile: req.body
            })

            res.send({message: "Character Profile Sucessfully Updated!"})
        } catch (error) {
            res.send(error)
        }
    }