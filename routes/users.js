// [SECTION] Dependencies and Modules
    const exp = require("express")
    const controller = require("./../controllers/users.js")
    const auth = require("../auth")
    const { verify } = auth

// [SECTION] Routing Component
    const route = exp.Router()

// [SECTION] POST
    // register user
    route.post('/register', controller.registerUser)

    // login user
    route.post('/login', controller.loginUser)
// [SECTION] GET
    // private to user
    route.get('/getUserDetails', verify, controller.getUserDetails)

    // public details
    route.get('/:id/getPublicUserDetails', controller.getPublicUserDetails)

    // get clothes/character profile
    route.get('/:email/getCharacterProfile', controller.getCharacterProfile)

// [SECTION] PUT
    //update character profile - will be attached to the save button
    route.put('/saveCharacterProfile', controller.saveCharacterProfile)

// [SECTION] Expose Route System
    module.exports = route