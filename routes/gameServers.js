// [SECTION] Dependencies and Modules
    const exp = require("express")
    const controller = require("../controllers/gameServers")
    const auth = require("../auth")
    const { verify, verifyAdmin} = auth

// [SECTION] Routing Component
    const route = exp.Router()

// [SECTION] POST
    // add server
    route.post('/add', verify, verifyAdmin, controller.addServer)

// [SECTION] GET
    // retrieve all servers
    route.get('/all', controller.getAllServer)

    // retrieve all available server
    route.get('/allAvailable', controller.getAllAvailableServer)

    // retrieve one available server
    route.get('/serverAvailable', controller.getAvailableServer)

// [SECTION] PUT
    // set isAvailable = true
    route.put('/:ipAddress/setAvailable', controller.setAvailable)

    // set isAvailable = false
    route.put('/:ipAddress/setUnavailable', controller.setUnavailable)

// [SECTION] DELETE
    // permanently delete a server from the database
    route.delete('/:ipAddress/deleteServer', controller.deleteServer)

// [SECTION] Expose Route System
    module.exports = route