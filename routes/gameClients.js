// [SECTION] Dependencies and Modules
    const exp = require("express")
    const controller = require("../controllers/gameClients")
    const auth = require("../auth")
    const { verify, verifyAdmin} = auth

// [SECTION] Routing Component
    const route = exp.Router()

// [SECTION] POST
    // add server
    route.post('/add', verify, verifyAdmin, controller.addServer)

// [SECTION] GET
    // retrieve all servers
    route.get('/all', controller.getAllServer)

    // retrieve specified ip
    route.get('/:ipAddress/specificIp', controller.getPlayerServer)

    // retrieve all available server
    route.get('/allAvailable', controller.getAllAvailableServer)

    // retrieve one available server
    route.get('/serverAvailable', controller.getAvailableServer)

// [SECTION] PUT
    // set isAvailable = true
    route.put('/:ipAddress/setAvailable', controller.setAvailable)

    // set isAvailable = false
    route.put('/:ipAddress/setUnavailable', controller.setUnavailable)

    // set isLoggingOut = true
    route.put('/:ipAddress/setLoggingOut', controller.setLoggingOut)

    // set isLoggingOut = false
    route.put('/:ipAddress/setLoggingOutToFalse', controller.setLoggingOutToFalse)

// [SECTION] DELETE
    // permanently delete a server from the database
    route.delete('/:ipAddress/deleteServer', controller.deleteServer)

// [SECTION] Expose Route System
    module.exports = route