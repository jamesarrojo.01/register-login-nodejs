// [SECTION] Dependencies and Modules
    const exp = require("express")
    const controller = require("../controllers/events")

// [SECTION] Routing Component
    const route = exp.Router()

// [SECTION] POST
    // create event
    route.post('/create', controller.createEvent)

// [SECTION] GET
    // retrieve all events
    route.get('/all', controller.getAllEvent)


// [SECTION] Expose Route System
    module.exports = route